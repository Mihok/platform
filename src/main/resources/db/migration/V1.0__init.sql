
-- 导出  表 bastionhost.t_audit_video 结构
CREATE TABLE IF NOT EXISTS `t_audit_video` (
  `id` bigint(20) NOT NULL,
  `targetid` bigint(20) DEFAULT NULL,
  `sn` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `deviceName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `path` varchar(128) COLLATE utf8_bin NOT NULL,
  `ctime` timestamp NULL DEFAULT NULL,
  `ip` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_audit_video_path` (`path`) USING BTREE,
  KEY `t_audit_video_username` (`username`) USING BTREE,
  KEY `t_audit_video_devicename` (`deviceName`) USING BTREE,
  KEY `t_audit_video_targetid` (`targetid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_config 结构
CREATE TABLE IF NOT EXISTS `t_config` (
  `id` bigint(20) unsigned zerofill NOT NULL,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='配置表';

-- 导出  表 bastionhost.t_device 结构
CREATE TABLE IF NOT EXISTS `t_device` (
  `id` bigint(20) NOT NULL,
  `sn` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `ip` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `devdesc` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `cpu` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `memony` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `sort` bigint(20) DEFAULT NULL,
  `os` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `isprober` enum('YES','NO') COLLATE utf8_bin DEFAULT NULL,
  `port` int(9) DEFAULT NULL,
  `protocol` enum('RDP','VNC','SSH','TELNET') COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_device_sn` (`sn`) USING BTREE,
  KEY `t_device_ctime` (`ctime`) USING BTREE,
  KEY `t_device_devdesc` (`devdesc`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_device_settings 结构
CREATE TABLE IF NOT EXISTS `t_device_settings` (
  `id` bigint(20) NOT NULL,
  `deviceId` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `autoRemote` enum('NO','YES') COLLATE utf8_bin DEFAULT NULL,
  `autoRun` enum('NO','YES') COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_device_settings_deviceid` (`deviceId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_log 结构
CREATE TABLE IF NOT EXISTS `t_log` (
  `id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `module` varchar(128) COLLATE utf8_bin NOT NULL,
  `content` varchar(1024) COLLATE utf8_bin NOT NULL,
  `ctime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `t_log_username` (`username`) USING BTREE,
  KEY `t_log_module` (`module`) USING BTREE,
  KEY `t_log_ctime` (`ctime`) USING BTREE,
  KEY `t_log_content` (`content`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_node 结构
CREATE TABLE IF NOT EXISTS `t_node` (
  `id` bigint(20) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `path` varchar(2048) DEFAULT NULL,
  `sort` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 bastionhost.t_node_device 结构
CREATE TABLE IF NOT EXISTS `t_node_device` (
  `id` bigint(20) NOT NULL,
  `nodeId` bigint(20) NOT NULL,
  `deviceId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nodeId_deviceId` (`nodeId`,`deviceId`),
  UNIQUE KEY `deviceId` (`deviceId`),
  KEY `nodeId` (`nodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点设备关系';

-- 导出  表 bastionhost.t_right 结构
CREATE TABLE IF NOT EXISTS `t_right` (
  `id` bigint(20) NOT NULL,
  `title` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `target` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_role 结构
CREATE TABLE IF NOT EXISTS `t_role` (
  `id` bigint(20) NOT NULL,
  `title` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 bastionhost.t_role_node 结构
CREATE TABLE IF NOT EXISTS `t_role_node` (
  `id` bigint(20) unsigned zerofill NOT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  `nodeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleId_nodeId` (`roleId`,`nodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 bastionhost.t_role_right 结构
CREATE TABLE IF NOT EXISTS `t_role_right` (
  `id` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  `rightId` bigint(20) NOT NULL,
  `rightTarget` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 bastionhost.t_role_user 结构
CREATE TABLE IF NOT EXISTS `t_role_user` (
  `id` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleId_userId` (`roleId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 bastionhost.t_tunnel 结构
CREATE TABLE IF NOT EXISTS `t_tunnel` (
  `id` bigint(20) NOT NULL,
  `uid` bigint(20) DEFAULT NULL,
  `deviceId` bigint(20) DEFAULT NULL,
  `tunnetType` enum('TCP','UDP') COLLATE utf8_bin DEFAULT NULL,
  `isactive` enum('NO','YES') COLLATE utf8_bin DEFAULT NULL,
  `localIp` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `localPort` int(9) DEFAULT NULL,
  `remotePort` int(9) DEFAULT NULL,
  `remarks` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_tunnel_remoteport` (`remotePort`) USING BTREE,
  KEY `t_tunnel_deviceId` (`deviceId`) USING BTREE,
  KEY `t_tunnel_uid` (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_user 结构
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` bigint(20) NOT NULL,
  `selfname` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_user_username` (`username`) USING BTREE,
  KEY `t_user_selfname` (`selfname`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 导出  表 bastionhost.t_user_web_session 结构
CREATE TABLE IF NOT EXISTS `t_user_web_session` (
  `id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `token` varchar(64) NOT NULL DEFAULT '',
  `ctime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='web会话控制';

INSERT INTO `t_role` (`id`, `title`) VALUES (0, '超级管理员');
INSERT INTO `t_right` (`id`, `title`, `target`) VALUES
	(1, '团队用户管理', 'team_user_manager'),
	(2, '团队角色管理', 'team_role_manager'),
	(3, '设备管理', 'device_device_manager'),
	(4, '设备查询', 'device_device_select'),
	(5, '设备节点管理', 'device_node_manager'),
	(6, '设备节点管理', 'device_node_select'),
	(7, '远程控制', 'device_remotecpe'),
	(8, '录像查询', 'browser_audit_video_list'),
	(9, '录像管理', 'browser_audit_video_manger');