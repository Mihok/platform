/**
 * 
 */
package com.desktop.web.uda.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.comenum.RemoteProtocolType;
import com.desktop.web.core.comenum.Status;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_device")
public class Device extends BaseEntity {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Boolean isonline;

    @TableField(exist = false)
    private Boolean isremote;

    @TableField(exist = false)
    private String usename;

    private String sn;
    private String ip;
    private String name;
    private String devdesc;
    private String cpu;
    private String memony;
    private String os;
    private Date ctime;
    private Long sort;
    private Status isprober;
    private Integer port;
    private RemoteProtocolType protocol;
    private String username;
    private String password;

    /**
     * @return the port
     */
    public Integer getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * @return the protocol
     */
    public RemoteProtocolType getProtocol() {
        return protocol;
    }

    /**
     * @param protocol the protocol to set
     */
    public void setProtocol(RemoteProtocolType protocol) {
        this.protocol = protocol;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the cpu
     */
    public String getCpu() {
        return cpu;
    }

    /**
     * @param cpu the cpu to set
     */
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    /**
     * @return the memony
     */
    public String getMemony() {
        return memony;
    }

    /**
     * @param memony the memony to set
     */
    public void setMemony(String memony) {
        this.memony = memony;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * @param ctime the ctime to set
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * @return the sn
     */
    public String getSn() {
        return sn;
    }

    /**
     * @param sn the sn to set
     */
    public void setSn(String sn) {
        this.sn = sn;
    }

    /**
     * @return the isonline
     */
    public Boolean getIsonline() {
        return isonline;
    }

    /**
     * @param isonline the isonline to set
     */
    public void setIsonline(Boolean isonline) {
        this.isonline = isonline;
    }

    /**
     * @return the devdesc
     */
    public String getDevdesc() {
        return devdesc;
    }

    /**
     * @param devdesc the devdesc to set
     */
    public void setDevdesc(String devdesc) {
        this.devdesc = devdesc;
    }

    /**
     * @return the isremote
     */
    public Boolean getIsremote() {
        return isremote;
    }

    /**
     * @param isremote the isremote to set
     */
    public void setIsremote(Boolean isremote) {
        this.isremote = isremote;
    }

    /**
     * @return the usename
     */
    public String getUsename() {
        return usename;
    }

    /**
     * @param usename the usename to set
     */
    public void setUsename(String usename) {
        this.usename = usename;
    }

    /**
     * @return the sort
     */
    public Long getSort() {
        return sort;
    }

    /**
     * @param sort the sort to set
     */
    public void setSort(Long sort) {
        this.sort = sort;
    }

    /**
     * @return the os
     */
    public String getOs() {
        return os;
    }

    /**
     * @param os the os to set
     */
    public void setOs(String os) {
        this.os = os;
    }

    /**
     * @return the isprober
     */
    public Status getIsprober() {
        return isprober;
    }

    /**
     * @param isprober the isprober to set
     */
    public void setIsprober(Status isprober) {
        this.isprober = isprober;
    }

}
