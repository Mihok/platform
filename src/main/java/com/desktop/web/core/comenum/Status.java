/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年12月13日 下午8:55:12  created
 */
package com.desktop.web.core.comenum;

/**
 * 
 *
 * @author baibai
 */
public enum Status {
    YES, NO
}
